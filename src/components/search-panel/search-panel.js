import React,{Component} from 'react';
export default class SearchPanel extends Component{

    searchChange = (e) => {
        const term = e.target.value;
        console.log('term');
        this.props.onChangeSearch(term);
    }
    render(){
        const {onChangeStatus} = this.props;
        return (
        <div>
            <input type="text" placeholder="search"
                   onChange={this.searchChange }
            />
        </div>
        );
    }
}
