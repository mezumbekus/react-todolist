import React from 'react';
import TodoListItem from "../todo-list-item/";
import './todo-list.css';

const TodoList = ({items, onDeletedItem,onImportantClick,onDoneClick}) => {
    const elems = items.map((elem) => {
        const {id,...itemProps } = elem;
        return (
            <li
                key={id}
                className="list-group-item"
            ><TodoListItem {...itemProps}
                           onDeletedItem={() => onDeletedItem(id)}
                           onImportantClick ={() => onImportantClick(id)}
                           onDoneClick = {() => onDoneClick(id)}
            />

            </li>
        );
    });

    return (
        <ul className="list-group todo-list">
            {elems}
        </ul>);
};

export default TodoList;