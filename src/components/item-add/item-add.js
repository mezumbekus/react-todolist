import React,{Component} from 'react';
import './item-add.css';

export default class ItemAdd extends Component {

    state = {
        label: ''
    };
    onLabel = (e) => {
        this.setState({
                label: e.target.value

        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.onAddItem(this.state.label);
        this.setState({
            label: ''
        })
    }

    render() {
        return (
          <form className="item-add"
            onSubmit={this.onSubmit}
          >
                <input type="text" placeholder="Название"
                    onChange={this.onLabel}
                       value={this.state.label}
                />
              <button
                  className="btn btn-primary"
              >Добавить</button>
          </form>
        );
    }
}