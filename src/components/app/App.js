import React,{Component} from 'react';
import AppHeader from "../../components/app-header/";
import TodoList from "../../components/todo-list/";
import SearchPanel from "../../components/search-panel/";
import ItemStatusFilter from "../../components/item-status-filter/";
import ItemAdd from "../../components/item-add/";

export default  class App extends Component{
    maxId = 100;
    state = {
        todoData: [

        ],
        term: '',
        status: 'all'
    };

    deleteItem(id) {
        const idx = this.state.todoData.findIndex((el) => {
            return el.id === id;
        });
        this.setState(({todoData}) => {
            const newArr = [
                    ...todoData.slice(0,idx),
                ...todoData.slice(idx+1)
            ];
            return {
                todoData: newArr
            }
        })
    }
    createItem(label) {
        return {
            label: label,
            important: false,
            id: this.maxId++
        }
    }
    addItem(label) {
        this.setState(() => {
           return this.state.todoData.push(this.createItem(label));
        });
    }

    search(items,term){
        if(term.length === 0){
            return this.state.todoData;
        }
        return items.filter((el) => {
            return el.label.indexOf(term) > -1 && el.status === this.state.status;
        })
    }
    filter(items,status) {
        switch (status) {
            case 'all':
                return items;
            case 'active':
                return items.filter((el) => !el.done);
            case 'done':
                return items.filter((el) => el.done);
        }
        return items;
    }
    onFilterChange = (status) =>{
       this.setState({
           status
       })
    }
    toggleProperty(arr,id,prop){

            const idx = arr.findIndex((el) => {
                return el.id === id;
            });
            const oldItem = arr[idx];
            const newItem = {
                ...oldItem,
                [prop]: !oldItem[prop]
            };
            return  [
                ...arr.slice(0,idx),
                newItem,
                ...arr.slice(idx+1)
            ];
    }
    toggleImportant(id) {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData,id,'important')
            }
        })
    }
    toggleDone(id) {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData,id,'done')
            }
        })
    }

    searchItem(text){
        this.setState({
            term: text
        })
    }
    render() {
        const {todoData,term,status} = this.state;
        const visibleItems = this.filter(this.search(todoData,term),status);
        return (<div>
            <AppHeader></AppHeader>
            <ItemAdd onAddItem={(data)=>{this.addItem(data)}}></ItemAdd>
            <SearchPanel onChangeSearch={(text) => {this.searchItem(text)}}>

            </SearchPanel>
            <ItemStatusFilter
                status={status}
                onFilterChange={this.onFilterChange}
            >
            </ItemStatusFilter>
            <TodoList items={visibleItems}
                      onDeletedItem={(id)=> this.deleteItem(id)}
                      onImportantClick={(id)=> this.toggleImportant(id)}
                      onDoneClick={(id)=> this.toggleDone(id)}
            ></TodoList>
        </div>);
    }

};
